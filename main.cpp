/*
  Name: main.cpp
  Copyright: Frank Dye, 11/2/2013
  Author: Frank Dye, ID: 912927332
  Description: A driver function to test getLargest, kthLargest and QuickSort
*/

#include <iostream>
#include <string>
#include <vector>
#include <random>

#include "functions.h"

using namespace std;

int main()
{

	string i;

	//Test vector contains all 
	vector<int> inVector;


	for (int i = 0; i < 150; i++)
	{
		int temp = (int) ceil(rand_FloatRange(0, 10000));
		inVector.push_back(temp);
	}

	string testString = "Some Stuff hello!";
	
	cout << "Array: ";
	for (int i = 0; i < inVector.size(); i ++)
	{
		cout << inVector[i] << " ";
	}

	cout << endl;
	cout << "Largest: " << getLargest(inVector, inVector.size()-1, 0) << endl;

	cout << "Reverse String: " << testString << endl;

	cout << "Reversed: " << reverseString(testString) << endl;

	cout << "Testing partition: ";
	for (int i = 0; i < inVector.size(); i ++)
	{
		cout << inVector[i] << " ";
	}

	cout << endl;


	int pivot =  (int) ceil(rand_FloatRange(0, inVector.size() -1));
	cout << "Pivot is : " << myPartition(inVector, 0, inVector.size()-1, pivot) << endl;
	cout << "Partitioned Array: ";

	for (int i = 0; i < inVector.size(); i ++)
	{
		cout << inVector[i] << " ";
	}

	cout << endl;


	cout << "Trying kthLargest: " << endl;
	cout << "Array: " ;
	for (int i = 0; i < inVector.size(); i ++)
	{
		cout << inVector[i] << " ";
	}
	cout << endl;

	for (int i = 1; i < 10; i++)
	{
		cout << "K: " << i << endl;
		cout << "K-thLargest: " << kthLargest(inVector, 0, inVector.size() -1, i) << endl;
	}

	cout << "\n\nQuickSorting Array: \n";

	for (int i = 0; i < inVector.size(); i++)
		cout << inVector[i] << " ";

	cout << "\nSorting....\n";

	QuickSort(inVector, 0, inVector.size()-1);

	for (int i = 0; i < inVector.size(); i++)
		cout << inVector[i] << " ";
	

	cin >> i;

	return 0;

}

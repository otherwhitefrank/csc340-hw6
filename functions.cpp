/*
  Name: functions.cpp
  Copyright: Frank Dye, 11/2/2013
  Author: Frank Dye, ID: 912927332
  Description: Implementation of QuickSort, largest, and kthLargest along with various helper functions. 
*/

#include "functions.h"
#include <iostream>
#include <string>
#include <random>

using namespace std;

//QuickSort algorithm, makes use of myPartition for partitioning step. Takes in a vector of objects and two indexes that
//indicates a range to sort the array over.
void QuickSort(vector<int>& arr, int left, int right)
{
	if (left != right)
	{
		int pivot = (int) ceil(rand_FloatRange(left, right));

		pivot = myPartition(arr, left, right, pivot);
		
		//Make sure it doesn't go under bounds
		int upperBound = pivot-1;
		if (upperBound < left)
			upperBound = left;

		//Quicksort left
		QuickSort(arr, left, upperBound);

		//Make sure pivot+1 doesn't go over bounds
		int lowerBound = pivot+1;
		if (lowerBound > right)
			lowerBound = right;

		//Quicksort right
		QuickSort(arr,lowerBound, right);
	}

}

//Helper function that swaps two elements of an array
void swapItem(vector<int>& arr, int i, int j)
{
	//Swap two items in our array
	int swap = 0;
	swap = arr[i];
	arr[i] = arr[j];
	arr[j] = swap;
}

//Helper function that returns a random number between a and b
float rand_FloatRange(float a, float b)
{
	return ((b-a)*((float)rand()/RAND_MAX))+a;
}

//Function to return kthLargest value in an array.
int kthLargest(vector<int>& arr, int left, int right, int k)
{

	if (left == right)
	{
		//Base case We are done return arr[left]
		return arr[left];
	}


	int pivot = (int) ceil(rand_FloatRange(left, right));

	pivot = myPartition(arr, left, right, pivot);

	int rank = right-pivot + 1;


	if (rank == k)
	{
		//Base case, if the size of our upper interval minus k is exactly zero then the pivot is the kth largest number
		return arr[pivot];
	}
	else if (k > rank)
	{
		//Value is in lower bounds 
		return kthLargest(arr, left, pivot - 1, k-rank);
	}
	else
	{
		//Value is in upper bounds.
		return kthLargest(arr, pivot + 1, right, k);
	}
}

//Helper function that partitions array based on a provided pivotIndex. After execution array is divided 
//into values less then pivot and value greater then pivot.
int myPartition(vector<int>& arr, int left, int right, int pivotIndex)
{
	int pivot = 0;
	int swap = 0;

	pivot = pivotIndex; 
    
	//First swap pivot with the right most array entry
	swapItem(arr, pivot, right);
	pivot = right;
	swap = left;
	for (int i = left; i < right; i++)
	{
		if(arr[i] < arr[pivot])
		{
			swapItem(arr, swap, i);
			swap++;
		}
	}

	swapItem(arr, right, swap);
	return swap;
}

//Function that uses recursion to print a string backwards.
string reverseString(string inString)
{
	int strLength = inString.length();

	if (strLength == 0)
		return inString;
	else
	{
		return inString.back() + reverseString(inString.substr(0, strLength-1));
	}
}

//Function to return the largest value from an array
int getLargest(vector<int> arr, int i, int max)
{
	int maxK = 0;

	if (arr[i] > max)
		maxK = arr[i];
	else
		maxK = max;


	if (i == 0)
	{
		return maxK;
	}
	else
	{
		return maxK = getLargest(arr, i-1, maxK);
	}
}